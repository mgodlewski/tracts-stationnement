![Securite Routière, vivre, ensemble](./img/logoSecuriteRoutiereR.jpg) ![Ville de Tours](./img/logoToursR.png)

Ce véhicule est arrêté ou stationné [^1] sur :

* ⬜ le trottoir, hors des places marqués
* ⬜ la bande cyclable
* ⬜ la chaussée

Il gêne le bon fonctionnement de la circulation.


C'est peut être un détail pour vous mais cela peut avoir de lourdes conséquences sur la sécurité des autres usagers :

⬜ Les piétons ne peuvent plus circuler sur les trottoirs ce qui les obligent à traverser hors des passages ou à circuler sur la chaussée, s'exposant au danger du trafic motorisé. Ces situations sont extrêmement dangereuses pour les enfants et les personnes à mobilité réduite.

⬜ La visibilité des autres usagers (piétons, cyclistes et automobilistes) est dégradée, particulièrement lorsque votre véhicule est stationné à proximité d'une intersection ou en amont des passages piétons. Cela peut conduite à de graves accidents corporels.

⬜ Les cyclistes ne peuvent plus circuler sur les bandes cyclables ce qui les obligent à s'insérer en urgence dans le trafic, situation extrèmenent inconfortable et accidentogène.


Pour ces raisons, vous êtes passible :

* ⬜ d'une contravention de catégorie 4 (135€) pour "stationnement très gênant" voir "dangereux" [^2]
* ⬜ d'un enlèvement à la fourrière entraînant des frais supplémentaires [^3]
* ⬜ de poursuite pénale en cas d'accident, votre responsabilité pouvant être retenue


La prochaine fois, merci de prendre quelques minutes pour trouver une place explicitement marquée. Il est aussi possible de stationner sur la chaussée, en étant attentif à ne pas gêner la circulation.

[^1]: https://www.securite-routiere.gouv.fr/reglementation-liee-la-route/arret-et-stationnement
[^2]: https://www.service-public.fr/particuliers/vosdroits/F34212
[^3]: https://www.service-public.fr/particuliers/vosdroits/F12918

