#!/bin/bash

image=$(cat .gitlab-ci.yml |yq -r .pages.image)

echo "FROM $image" > Dockerfile
cat .gitlab-ci.yml |yq -r .pages.before_script[] | sed 's/\(.*\)/RUN \1/' >> Dockerfile
echo "COPY . /app">> Dockerfile
echo "WORKDIR /app" >> Dockerfile

docker build -t mgodlewski/tracts-stationnement .

docker run -v $(pwd)/public:/app/public --rm mgodlewski/tracts-stationnement /app/run.sh public

