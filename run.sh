#!/bin/bash

set -euo pipefail

[ $# -ne 1 ] && echo "usage: $0 buildDirectory" && exit 1

buildDirectory=$1

[ ! -d "$buildDirectory" ] && mkdir $buildDirectory

DEBUG=$buildDirectory/debug.log

generatePreview() {
  tract=$1
  dpi=${2:-25}
  basename=$(basename $tract .svg)
  preview=${buildDirectory}/$basename.png
  inkscape -f $tract -d $dpi -e $preview >> $DEBUG 2>&1
  echo $basename.png
}

generate3xPage() {
  tract=$1
  basename=$(basename $tract .svg)
  pdf=${buildDirectory}/$basename.pdf
  pdfx3=${buildDirectory}/${basename}x3.pdf
  inkscape -f $tract -d 300 -A $pdf >> $DEBUG 2>&1
  pdfjam $pdf $pdf $pdf --frame true --nup 1x3 --outfile $pdfx3 >> $DEBUG 2>&1
  echo $pdfx3
}

generateRectoVerso() {
  recto=$1
  verso=$2
  rectoVerso=${buildDirectory}/$(basename $recto .pdf)_$(basename $verso .pdf).pdf
  pdftk  A=$recto B=$verso cat A1 B1 output $rectoVerso >> $DEBUG 2>&1
  echo $rectoVerso
}

verso=$(generate3xPage Verso.svg)

verso_preview=$(generatePreview Verso.svg 100)

#Truncate index.html
cat <<EOF > $buildDirectory/index.html
<html>
<head>
  <title>Stationnement très gênant : les tracts</title>
  <style>
figure {
  text-align: center;
  font-style: italic;
  font-size: smaller;
  text-indent: 0;
  margin: 0.5em;
  padding: 0.5em;
}
  </style>
</head>
<body style="display: flex; flex-wrap: wrap;">
<div style="width: 90vw; display: flex; justify-content: center;">
<h1>Stationnement très gênant : les tracts</h1>
</div>
<div style="width: 90vw; display: flex; justify-content: center;">
<figure>
  <picture>
    <img src="$verso_preview"/>
  </picture>
  <figcaption>Verso des tracts</figcaption>
</figure>
</div>
EOF

for f in $(ls -1 *.svg|grep -v -e Verso.svg -e Base.svg)
do
  echo "$f"
  recto=$(generate3xPage $f)
  output=$(generateRectoVerso $recto $verso)
  tract_href=$(basename $output)
  tract=$(basename $f .svg | sed 's/\([A-Z0-9]\)/ \1/g')
  preview=$(generatePreview $f)
  echo '<div style="width: 30vw; display: flex; justify-content: flex-end;"><a href="'$tract_href'"><figure><picture><img src="'$preview'"/></picture><figcaption>'$tract'</figcaption></figure></a></div>' >> $buildDirectory/index.html
done

echo '</body></html>' >> $buildDirectory/index.html

